#!/usr/bin/env node


// Includes
// ================

var argv = require('optimist')
                .usage('Usage: ./mysqldumpcsv --host [MySQL server] --user [MySQL user] --password [MySQL password]')
                .demand(['host','user','password'])
                .argv;


// Run the export
var s = require('./main.js').create().main(argv.host, argv.user, argv.password);

