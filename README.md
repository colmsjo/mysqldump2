Introcuction
------------

The standard mysqldump tool requires FILE privileges which often isn't 
availbe (for example when using Amazon RDS). This tool will dump a 
MySQL database into csv files. It will also save the tables into JSON
files. In addition is a JSON file with the database structure saved. 

Make sure that the folders `./out/csv` and `./out/json` exists before
running the tool.


Usage
-----

Run like this:

	>./mysqldump2.sh --host [MySQL IP] --user [MySQL user] --password [MySQL password]


Development
-----------

I'm using docker for development and testing. Instructions on howto install is found [here](https://docs.docker.com/) 

Create a MySQL database with test data like this:

	>docker build .
	>docker run -d [docker image ID]


Get the IP of the MySQL host, i.e docker container:

	>docker inspect [docker container ID]

Run the script:
	>./mysqldump2.sh --host [docker container ID] --user test --password test


TODO
---

 * Create tables using the JSON file in a SAP HANA database

