(function(exports) {

// main.js
//------------------------------
//
// 2014-07-20, Jonas Colmsjö
//
// Copyright Gizur AB 2014
//
//
// Export MySQL databsase into CSV file. A JSON.file with the database structure is
// also created. 
// 
// use like this:
// var s = require('./main.js').create().main('host', 'user', 'password');
//
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------


"use strict";

exports.create = function() {

  return {

    // Configuration
    //--------------

    // Filename for database structure
    _output_path_struct            : './out/mysqldb.json',
    // Path for CSV files
    _output_path_csv               : './out/csv/',
    // Path for JSON files
    _output_path_json              : './out/json/',
    // Separator
    _csv_separator                 : ';',

    // Available columns:
    //
    // TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, COLUMN_DEFAULT,
    // IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, CHARACTER_OCTET_LENGTH, NUMERIC_PRECISION
    // NUMERIC_SCALE, CHARACTER_SET_NAME, COLLATION_NAME, COLUMN_TYPE, COLUMN_KEY, EXTRA, PRIVILEGES                      | COLUMN_COMMENT 
    _information_schema_columns : 'column_name, data_type',


    // Class properties
    // ----------------

    // MySQL system tables we're not interested in
    _system_tables : ["CHARACTER_SETS","COLLATIONS","COLLATION_CHARACTER_SET_APPLICABILITY","COLUMNS",
                      "COLUMN_PRIVILEGES","ENGINES","EVENTS","FILES","GLOBAL_STATUS","GLOBAL_VARIABLES",
                      "KEY_COLUMN_USAGE","PARAMETERS","PARTITIONS","PLUGINS","PROCESSLIST","PROFILING",
                      "REFERENTIAL_CONSTRAINTS","ROUTINES","SCHEMATA","SCHEMA_PRIVILEGES","SESSION_STATUS",
                      "SESSION_VARIABLES","STATISTICS","TABLES","TABLESPACES","TABLE_CONSTRAINTS",
                      "TABLE_PRIVILEGES","TRIGGERS","USER_PRIVILEGES","VIEWS","INNODB_CMP_RESET",
                      "INNODB_TRX","INNODB_CMPMEM_RESET","INNODB_LOCK_WAITS","INNODB_CMPMEM","INNODB_CMP",
                      "INNODB_LOCKS"],

    _mysql_cred : {
          host     : '',
          user     : '',
          password : ''
        },
    _columns    : [],
    _tables     : [],


    // Helper functions
    // ----------------

    clone : function (a) {
         return JSON.parse(JSON.stringify(a));
    },


    // Append the columns for table_name into _columns 
    // ------------------------------------------------

    getColumns : function(table_name, cb) {
      var mysql      = require('mysql');
      var connection = mysql.createConnection(this._mysql_cred);

      connection.query(
        "select "+this._information_schema_columns+" from information_schema.columns where table_name = '"+table_name+"'", 
        function(err, rows, fields) {
          if (err) throw err;

          // save the columns
          this._columns.push( { table_name : table_name, columns : this.clone(rows) });

          connection.end();

          cb(null, 'getColumns');
        }.bind(this)
      );
    },


    // Fetch columns for all tables in _tables into _columns 
    // -----------------------------------------------------

    getAllColumns : function(cb) {
      var u     = require('underscore');
      var async = require('async');

      async.mapSeries(
        this._tables, 
        this.getColumns.bind(this), 
        function(err, result){
          if (err) throw err;
          cb(null, 'getAllColumns');
        }
      );

    },


    // Fetch all tables into  _tables
    // ------------------------------

    getTables : function(cb) {
      var mysql      = require('mysql');
      var connection = mysql.createConnection(this._mysql_cred);
      var u          = require('underscore');

      connection.query(
//        "select table_name from information_schema.tables where table_schema="+ "'"+this._mysql_cred.schema+"'", 
        "select table_name from information_schema.tables", 
        function(err, rows, fields) {
          if (err) throw err;

          // save the array of tables
          this._tables = u.map(rows, function(o){return o.table_name});
          
          // filter out the system tables
          this._tables = u.filter(this._tables, function(i){return !u.contains(this._system_tables, i);}.bind(this));

          connection.end();

          cb(null, 'getTables');
        }.bind(this)
      );
    },


    // Export table 
    // ------------------------------

    exportTable : function(table, cb) {
      var u                     = require('underscore');
      var fs                    = require('fs');
      var mysql                 = require('mysql');

      this._mysql_cred.database = this._mysql_cred.user;
      var connection            = mysql.createConnection(this._mysql_cred);

      /*
      var query      = connection.query("select * from "+table);
      query
        .stream({highWaterMark: 100})

        .pipe(fs.createWriteStream(this._output_path_json+table+'.json'))

        .on('error', function(err) {
          throw err;
        })

        // all rows have been received
        .on('end', function() {
          connection.end();
          cb(null, 'exportTable');
        }.bind(this));*/

      connection.query(
        "select * from "+table,

        function(err, rows, fields) {
          if (err) throw err;

          // Save the JSON file
          var fs   = require('fs'),
              path = this._output_path_json+table+'.json';

          fs.writeFile(path, JSON.stringify(rows, null, 4), function(err) {
            if(err) {
              console.log(err);
            } else {
              console.log("JSON for "+table+" saved to " + path);
            }
          }.bind(this)); 

          // Save the CSV file
          var fs   = require('fs'),
              path = this._output_path_csv+table+'.csv';

          var csv = "";

          // cleanout strange functions that are exported otherwise
          rows = JSON.parse(JSON.stringify(rows));
          
          for (var key in rows[0]) csv += key + this._csv_separator;
          csv += "\n";
          for (var i=0; i<rows.length; i++) {
            for (var key in rows[i]) csv += rows[i][key] + this._csv_separator;
            csv += "\n";
          }

          fs.writeFile(path, csv, function(err) {
            if(err) {
              console.log(err);
            } else {
              console.log("CSV for "+table+" saved to " + path);
            }
          }.bind(this)); 

          connection.end();
          cb(null, 'exportTable');
        }.bind(this)
      );


    },


    // Export all tables 
    // ------------------------------

    exportAllTables : function(cb) {
      var u     = require('underscore');
      var async = require('async');

      async.mapSeries(
        this._tables, 
        this.exportTable.bind(this), 
        function(err, result){
          if (err) throw err;
          cb(null, 'exportAllTables');
        }
      );

    },


    // Main procedure, runs everything in a sequence using the async library
    // ---------------------------------------------------------------------

    main : function(host, user, password) {
      var async = require('async');

      this._mysql_cred.host     = host;
      this._mysql_cred.user     = user;
      this._mysql_cred.password = password;

      async.series([
        function(cb){
          this.getTables(cb);
        }.bind(this),

        function(cb){
          this.getAllColumns(cb);
        }.bind(this),

        function(cb){
          var fs = require('fs');

          fs.writeFile(this._output_path_struct, JSON.stringify(this._columns, null, 4), function(err) {
            if(err) {
              console.log(err);
            } else {
              console.log("JSON saved to " + this._output_path_struct);
            }
            cb(null, 'end')
          }.bind(this)); 

        }.bind(this),

        function(cb){
          this.exportAllTables(cb);
        }.bind(this),

        ],
        function(err, result){
          console.log(JSON.stringify(result));
        }

      );
    }

  };
};

}(typeof exports === 'undefined' ? this['mysqldumpcsv']={} : exports));
